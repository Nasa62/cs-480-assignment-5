// Assignment 5
// Repository: https://bitbucket.org/Nasa62/cs-480-assignment-5
// - A multithreaded flat-file database thingy.
//
// File: dbMain.c
// Author: Chad Dulake
// - Main application, invokes multiple threads of dbThread.
//
// #######################
// # COMPILE INFORMATION #
// ####################### 
// YOU MUST compile dbThread.c as dbThread.exe (main depends on it)
// You may compile dbMain.c under whatever name you want.
// If you have problems Andrew, I compiled using these exact settings with GCC 3.4.4 in Cygwin:
//  gcc -std=c99 -o dbThread.exe dbThread.c
//  gcc -std=c99 -o main.exe dbMain.c
//

#include "db.h"
#include <wait.h>

const int maxThreads = 3;

/* main()
 * Provide the name of the dbThread executable as the first argument.
 * Creates 3 instances of dbThread and runs them.
 */
int main(int argc, char* argv[])
{
	// Cleanup from previous runs
	remove(databaseFilePath);
	remove(lockFilePath);

	pid_t childrenPIDs[maxThreads];
	for(int i = 0; i < maxThreads; i++)
	{
		if((childrenPIDs[i] = fork()) < 0) // Fork & test for error
		{
			fprintf(stderr, "Fork error\n");
			abort();
		}
		else if(childrenPIDs[i] == 0) // In Child
		{
			if(execlp("./dbThread.exe", "dbThread.exe", NULL) < 0) // Execute process & test for error
			{
				fprintf(stderr, "dbThread execution error!\n");
				exit(1);
			}
			exit(0);
		}
	}

	// Wait for children to complete
	for(int i = 0; i < maxThreads; i++)
		wait(NULL);

	// Open & print the database
	if((databaseFileDescriptor = open(databaseFilePath, O_RDONLY | O_BINARY)) < 0)
	{
		fprintf(stderr, "Error opening database file for reading\n");
		return 1;
	}
	printDB();
	return 0;
}
