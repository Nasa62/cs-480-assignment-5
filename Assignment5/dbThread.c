// Assignment 5
// Repository: https://bitbucket.org/Nasa62/cs-480-assignment-5
//
// File: dbThread.c
// Author: Chad Dulake
// - Database processing file, this does most of the work, it should not be executed directly.
//
// See dbMain.c for more information.
//

#include "db.h"
#include <strings.h>

// Uncommenting this next line will cause dbThread to print out every addition & removal of a person to the database.
//#define VERBOSE 

const Person nullPerson = {0};

void add(Person* person);
void writeLine(int line, Person* person);
int get(char* name);
void removePerson(char* name);

/* main()
 * Adds 10 Persons to the database then removes 9 Persons from the database.
 */
int main(int argc, char* argv[])
{
	// Attempt to create lockfile, or wait until we can create it.
	int lockFileDescriptor;
	do lockFileDescriptor = open(lockFilePath, O_CREAT | O_EXCL, 0600);
	while(lockFileDescriptor < 0);

	/* Critical Section */
	if((databaseFileDescriptor = open(databaseFilePath, O_RDWR | O_BINARY | O_CREAT, 0644)) < 0)
	{
		fprintf(stderr, "Error opening/creating database file for reading & writing\n");
		close(lockFileDescriptor);
		remove(lockFilePath);
		return 1;
	}

	Person* newPerson = calloc(1, sizeof(Person));
	newPerson->id = getpid();
	sprintf(newPerson->name, "Process_%u", newPerson->id);
	for(int i = 0; i < 10; i++)
		add(newPerson);
	for(int i = 0; i < 9; i++)
		removePerson(newPerson->name);
	free(newPerson);

	close(databaseFileDescriptor);
	/* End critical section */

	close(lockFileDescriptor);
	remove(lockFilePath);
	return 0;
}

/* add(Person)
 * Inserts a person into the database
 * The person's name & number should be printed to stdout letting the user know they have been added successfully.
 */
void add(Person* person)
{
	writeLine(-1, person); // Append the new person to the end of the file
#ifdef VERBOSE
	fprintf(stdout, "ADD    - ");
	printPerson(person);
#endif
}

/* writeLine()
 * writes the specified line to the file
 * Line numbers start at 0
 * Line number of -1 means append, -2 means overwrite last line.
 * Line number of 0 means write first line, 1 means write second line.
 */
void writeLine(int line, Person* person)
{
	int seekMode = line >= 0 ? SEEK_SET : SEEK_END;
	if(line < 0)
		line++;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), seekMode) < 0)
		return; // Error

	if(write(databaseFileDescriptor, person, sizeof(Person)) < 0)
		return; // Error
}

/* get(char* name)
 * Retrieves the person's id number.
 * Main function should print the number returned to STDOUT.
 * Returns -1 if no result.
 */
int get(char* name)
{
	Person* personBuffer;
	int readBytes;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		if(strcasecmp(personBuffer->name, name) == 0)
		{
			int result = personBuffer->id;
			free(personBuffer);
			return result;
		}

		free(personBuffer);
	}
	return -1;
}

/* removePerson(char* name)
 * name must be a C String.
 * name is case-insensitive.
 * Removes the person from the database & compacts the gap where the record was.
 * The person's name & id number is printed to stdout if removal was successful.
 */
void removePerson(char* name)
{
	int readBytes;
	Person* personBuffer;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break; // EOF
		if(strcasecmp(personBuffer->name, name) != 0)
		{
			free(personBuffer);
			continue;
		}
#ifdef VERBOSE
		fprintf(stdout, "REMOVE - ");
		printPerson(personBuffer);
#endif
		free(personBuffer);
		for(int j = 0; 1; j++)
		{
			personBuffer = readLine(i + j + 1);
			if(personBuffer == NULL)
			{
				int fSize = lseek(databaseFileDescriptor, -sizeof(Person), SEEK_END);
				ftruncate(databaseFileDescriptor, fSize);
				return; // EOF
			}
			writeLine(i + j, personBuffer);
			free(personBuffer);
		}
	}
}
