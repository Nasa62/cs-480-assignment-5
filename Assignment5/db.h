// Assignment 5
// Repository: https://bitbucket.org/Nasa62/cs-480-assignment-5
//
// File: db.h
// Author: Chad Dulake
// - Shared resources fot the database thread & main program.
//
// See dbMain.c for more information.
//

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

#define person_nameSize 28

typedef struct Person Person;

struct Person
{
	unsigned int id;
	char name[person_nameSize];
};

const char databaseFilePath[] = "./dulake.bin";
const char lockFilePath[] = "./.dulake.bin.lck";
int databaseFileDescriptor;

Person* readLine(int line);
void printPerson(Person* person);
void printDB();

/* readLine()
 * reads the specified line from the file
 * returns NULL if past end of file or out of bounds.
 * returns the Person read from specified line
 * Assumes the database being read was written by the same version of this program (has the same sizes)
 */
Person* readLine(int line)
{
	Person* result = (Person*) malloc(sizeof(Person));
	int readBytes;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), SEEK_SET) < 0)
		return NULL; // EOF/Out of bounds
	readBytes = read(databaseFileDescriptor, result, sizeof(Person));
	if(readBytes == 0)
		return NULL; // EOF
	if(readBytes != sizeof(Person))
		fprintf(stderr, "Warning # of bytes read does not match the size of a Person\n");

	return result;
}

/* printPerson()
 * Prints the person's information to stdout
 * Assures proper handling if the name is not a null terminated string
 */
void printPerson(Person* person)
{
	fprintf(stdout, "ID: %u, NAME: ", person->id);
	fwrite(person->name, sizeof(char), person_nameSize, stdout);
	fprintf(stdout, "\n");
}

/* printDB()
 * prints the name & id numbers of everyone in the database
 */
void printDB()
{
	Person* personBuffer;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		printPerson(personBuffer);
		free(personBuffer);
	}
}
